package com.projek.farah.projek3.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by FARAH on 21/10/2016.
 */

public class ModePhoto {

    @SerializedName("nama") public String nama;
    @SerializedName("foto") public String foto;
}
