package com.projek.farah.projek3;

import com.projek.farah.projek3.model.ModePhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by FARAH on 23/10/2016.
 */

public interface BaseAPI {
    String BaseUrl = "http://demo7104902.mockable.io/";

    @GET("dedek")
    Call<List<ModePhoto>> getPhotoList();

    class Factory{
        public static BaseAPI create(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(BaseAPI.class);
        }
    }
}
