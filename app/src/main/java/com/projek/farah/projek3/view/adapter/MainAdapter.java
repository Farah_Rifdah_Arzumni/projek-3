package com.projek.farah.projek3.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projek.farah.projek3.R;
import com.projek.farah.projek3.model.ModePhoto;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by FARAH on 21/10/2016.
 */

public class MainAdapter extends BaseAdapter {
    List<ModePhoto> photos;
    Context context;
    LayoutInflater interflater;

    public MainAdapter(Context context, List<ModePhoto> photos) {
        this.photos = photos;
        this.context = context;
        this.interflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int i) {
        return photos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (interflater == null){
            interflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    }
        if (view == null){
            view = interflater.inflate(R.layout.item_listview,null);
        }
        ImageView foto = (ImageView) view.findViewById(R.id.item_foto);
        TextView nama = (TextView) view.findViewById(R.id.item_nama);

        Picasso.with(context).load(photos.get(i).foto).into(foto);
        final String stringnama = photos.get(i).nama;
        nama.setText(stringnama);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "NAMA:\n"+stringnama, Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
