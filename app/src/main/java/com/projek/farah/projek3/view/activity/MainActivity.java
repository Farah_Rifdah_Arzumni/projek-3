package com.projek.farah.projek3.view.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.projek.farah.projek3.BaseAPI;
import com.projek.farah.projek3.R;
import com.projek.farah.projek3.model.ModePhoto;
import com.projek.farah.projek3.view.adapter.MainAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    BaseAPI baseAPI;
    ListView listView;
    ProgressBar progressBar;
    SwipeRefreshLayout refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        baseAPI = BaseAPI.Factory.create();
        assignUIElements();
        assignUIEvent();
        loadPhoto();
    }

    private void assignUIElements(){
        listView = (ListView) findViewById(R.id.listview);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);
        refresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
    }

    private void assignUIEvent(){
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPhoto();
            }
        });
    }

    private void loadPhoto (){
        showProgress();
        Call<List<ModePhoto>> photoList = baseAPI.getPhotoList();
        photoList.enqueue(new Callback<List<ModePhoto>>() {
            @Override
            public void onResponse(Call<List<ModePhoto>> call, Response<List<ModePhoto>> response) {

                MainAdapter adapter = new MainAdapter(MainActivity.this, response.body());
                listView.setAdapter(adapter);
                closeProgress();
            }

            @Override
            public void onFailure(Call<List<ModePhoto>> call, Throwable t) {
                System.out.println(">> GAGAL LOAD PHOTO");
                System.out.println(">> "+t.getMessage());
            }
        });

    }

    private void showProgress (){
        if (!refresh.isRefreshing()){
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void closeProgress(){
        progressBar.setVisibility(View.GONE);
        refresh.setRefreshing(false);
    }

}
